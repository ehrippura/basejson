//
//  Base64View.swift
//  BaseJSON
//
//  Created by Wayne Lin on 2015/3/16.
//  Copyright (c) 2015年 EternalWind. All rights reserved.
//

import Cocoa

class Base64View: NSView {

    @IBOutlet var encodeView: NSTextView!
    @IBOutlet var decodeView: NSTextView!

    override func awakeFromNib() {

        encodeView.isAutomaticQuoteSubstitutionEnabled = false
        encodeView.isAutomaticDashSubstitutionEnabled = false
        encodeView.isAutomaticTextReplacementEnabled = false
        encodeView.isAutomaticTextCompletionEnabled = false

        decodeView.isAutomaticQuoteSubstitutionEnabled = false
        decodeView.isAutomaticDashSubstitutionEnabled = false
        decodeView.isAutomaticTextReplacementEnabled = false
        decodeView.isAutomaticTextCompletionEnabled = false

        let font = NSFont(name: "Menlo", size: 12)

        decodeView.font = font
        encodeView.font = font
    }
}
