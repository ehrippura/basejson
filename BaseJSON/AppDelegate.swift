//
//  AppDelegate.swift
//  BaseJSON
//
//  Created by Wayne Lin on 2015/3/16.
//  Copyright (c) 2015年 EternalWind. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return sender == NSApp
    }
}

