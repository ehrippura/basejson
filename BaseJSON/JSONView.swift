//
//  JSONView.swift
//  BaseJSON
//
//  Created by Wayne Lin on 2015/3/16.
//  Copyright (c) 2015年 EternalWind. All rights reserved.
//

import Cocoa

class JSONView: NSView {
    @IBOutlet var sourceView: NSTextView!
    @IBOutlet var verifiedView: NSTextView!

    override func awakeFromNib() {

        sourceView.isAutomaticQuoteSubstitutionEnabled = false
        sourceView.isAutomaticDashSubstitutionEnabled = false
        sourceView.isAutomaticTextReplacementEnabled = false
        sourceView.isAutomaticTextCompletionEnabled = false

        verifiedView.isAutomaticQuoteSubstitutionEnabled = false
        verifiedView.isAutomaticDashSubstitutionEnabled = false
        verifiedView.isAutomaticTextReplacementEnabled = false
        verifiedView.isAutomaticTextCompletionEnabled = false

        let font = NSFont(name: "Menlo", size: 12)

        sourceView.font = font
        verifiedView.font = font
    }
}
