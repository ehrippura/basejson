//
//  AppController.swift
//  BaseJSON
//
//  Created by Wayne Lin on 2015/3/16.
//  Copyright (c) 2015年 EternalWind. All rights reserved.
//

import Cocoa

class AppController: NSObject {

    @IBOutlet var jsonView: JSONView!
    @IBOutlet var base64View: Base64View!
    @IBOutlet var box: NSBox!

    var views: [NSView]!

    override func awakeFromNib() {
        views = [base64View, jsonView]
        switchToView(base64View)
    }

    @IBAction func switchValueChanged(_ sender: NSSegmentedControl) {

        for view in views {
            view.removeFromSuperview()
        }

        let selectedView = views[sender.selectedSegment]
        switchToView(selectedView)
    }

    private func switchToView(_ view: NSView) {
        view.frame = NSRectFromCGRect(NSRectToCGRect(box.bounds).insetBy(dx: 20, dy: 20))
        view.autoresizingMask = [.width, .height]
        box.addSubview(view)
    }

    @IBAction func verifyJSON(_ sender: NSButton) {
        if let data = jsonView.sourceView.string.data(using: .utf8) {
            if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) {
                let formatData = try! JSONSerialization.data(withJSONObject: jsonData, options: [.prettyPrinted])
                let formattedJSON = String(data: formatData, encoding: .utf8)!
                jsonView.verifiedView.string = formattedJSON
            } else {
                jsonView.verifiedView.string = "Not JSON"
            }
        }
    }

    @IBAction func encodeBase64(_ sender: NSButton) {
        if let data = base64View.encodeView.string.data(using: .utf8) {
            let str = data.base64EncodedString()
            base64View.decodeView.string = str
        }
    }

    @IBAction func decodeBase64(_ sender: NSButton) {
        if let data = Data(base64Encoded: base64View.decodeView.string) {
            if let string = String(data: data, encoding: .utf8) {
                base64View.encodeView.string = string
            } else {
                var str = "binary( "
                for byte in data {
                    str.append(String(format: "%02X", byte))
                }
                str.append(" )")
                base64View.encodeView.string = str
            }
        }
    }
}
